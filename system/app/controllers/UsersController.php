<?php

class UsersController extends \BaseController {

    public function login() {
        return View::make('users.login');
    }

    public function index() {

//        $users = new User();
//        Mail::send('emails.reminder', array('tekst' => 'sdasdasds'), function($message) use($user) {
////                    $message->to($user->email, $user->imie." ".$user->nazwisko)->subject('Reminder!');
//            $message->to('vouss121@gmail.com', $user->imie." ".$user->nazwisko)->subject('Reminder!');
//        });
//        return View::make('pages.reminders.all')->with('reminders', Reminder::with('users')->get());

        Mail::send('emails.reminder', array('tekst' => 'sdasdasds'), function($message) {
            $message->to('vouss121@gmail.com', 'John Smith')->subject('Welcome!');
        });

        return View::make('pages.users.index')->with('users', User::with('groups')->get());
    }

    public function add() {
        if(Request::isMethod('get')) {
            $groups = new Group();
            return View::make('pages.users.add')->with('groups', $groups->showAll(1));
        } elseif(Request::isMethod('post')) {

            $input = Input::all();

            $validator = Validator::make($input, array(
                'name'   => 'required',
                'surname'=> 'required',
                'phone'  => 'required',
                'grupa'  => 'required',
                'email'  => 'required',
                'pass1'  => 'required',
                'pass2'  => 'required|same:pass1'
            ));

            if($validator->fails()) {
                return Redirect::to('users/add')->withErrors($validator);
            }

            $user = New User;
            $user->imie = Input::get('name');
            $user->nazwisko = Input::get('surname');
            $user->telefon = Input::get('phone');
            $user->email = Input::get('email');
            $user->remember_token = Input::get('remember_token');
            $user->password = Hash::make(Input::get('pass2'));
            $user->id_grupy = Input::get('grupa');
            $user->created_at = new \DateTime;
            $user->updated_at= new \DateTime;
            $user->save();

            return Redirect::to('users');
        }
    }
}


