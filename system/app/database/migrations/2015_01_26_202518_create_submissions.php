<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('zgloszenia', function(Blueprint $table) {
            $table->increments('id_zgloszenia');
            $table -> integer('id_uzytkownika')->unsigned();
            $table->date('data_utworzenia');
            $table->integer('id_uslugi')->unsigned();
            $table->timestamps();
        });

        Schema::table('zgloszenia', function($table) {
            $table->foreign('id_uzytkownika')->references('id_uzytkownika')->on('uzytkownicy');
            $table->foreign('id_uslugi') -> references('id_uslugi')->on('uslugi');
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
