<?php

class OrdersController extends BaseController {

    protected $layout = 'layouts.system';
    public function show()
    {
        return View::make('pages.orders.all');
    }

}
