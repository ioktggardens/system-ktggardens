e@extends('layouts.system')
@section('content')
    <section class="wrapper">
        {{--<h3><i class="fa fa-angle-right"></i>Users</h3>--}}
        <div class="row mt">
            <div class="col-lg-12">
                <div class="content-panel">
                    <h4><i class="fa fa-angle-right"></i> All users in system</h4>
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed text-center">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Phone</th>
                                    <th>Group</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {{--{{ var_dump($users) }}--}}
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id_uzytkownika }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->imie }}</td>
                                    <td>{{ $user->nazwisko }}</td>
                                    <td>{{ $user->telefon }}</td>
                                    <td>{{ $user->groups->nazwa }}</td>
                                    <td>{{ $user->created_at}}</td>
                                    <td><a onclick="(confirm('Are you sure?'))? window.location='{{ URL::Route('deleteUser',$user->id_uzytkownika)}}' : false "><i class="fa fa-times tooltips" data-original-title="Remove"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </section>
                </div><!-- /content-panel -->
            </div><!-- /col-lg-4 -->
        </div><!-- /row -->
    </section><! --/wrapper -->
@stop