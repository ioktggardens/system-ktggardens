<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'uzytkownicy';
    protected $primaryKey = 'id_uzytkownika';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    public function reminders() {
        return $this->hasMany('User', 'id_uzytkownika', 'id_uzytkownika');
    }
    public function groups() {
        return $this->belongsTo('Group', 'id_grupy', 'id_grupy');
    }

    public function showAll($list = null) {
        if($list) {
            return User::lists('nazwisko', 'id_uzytkownika');
        }

        return User::all();
    }
}
