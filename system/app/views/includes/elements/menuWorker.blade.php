<p class="centered"><a href="#">{{ HTML::image('assets/img/ui-sam.jpg',null, array('class' => 'img-circle', 'width' => '60')) }}</a></p>
<h5 class="centered">{{ Auth::user()->nazwisko }} {{ Auth::user()->imie }}</h5>
<h6 class="centered">Admin</h6>

<li class="mt">
    <a href="{{ URL::to('/zgloszenia') }}">
        <i class="fa fa-dashboard"></i>
        <span>SYSTEM</span>
    </a>
</li>
{{--<li class="sub-menu">--}}
    {{--<a class="dcjq-parent">--}}
        {{--<i class="fa fa-user"></i>--}}
        {{--<span>Manage Users</span>--}}
    {{--</a>--}}
    {{--<ul class="sub">--}}
        {{--<li><a  href="{{ URL::to('/users') }}">Users</a></li>--}}
        {{--<li><a  href="{{ URL::to('/users/add') }}">Add user</a></li>--}}
    {{--</ul>--}}
{{--</li>--}}
{{--<li class="sub-menu">--}}
    {{--<a class="dcjq-parent">--}}
        {{--<i class="fa fa-th"></i>--}}
        {{--<span>Manage Reminders</span>--}}
    {{--</a>--}}
    {{--<ul class="sub">--}}
        {{--<li><a  href="{{ URL::to('/reminders') }}">Reminders</a></li>--}}
        {{--<li><a  href="{{ URL::to('/reminders/add') }}">Add reminder</a></li>--}}
    {{--</ul>--}}
{{--</li>--}}
