<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendMailCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:sendmail';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire() {
        echo("sending..");
        //cron
        ///Applications/MAMP/bin/php/php5.5.14/bin/php /Applications/MAMP/htdocs/systemktg/system/artisan command:sendmail
		$reminders = Reminder::All();
        foreach($reminders as $reminder) {
            $givenDate = $reminder -> data_przypomnienia;
            $data['rok'] = intval(substr($givenDate, 0, 4));
            $data['miesiac'] = intval(substr($givenDate, 5, 2));
            $data['dzien'] = intval(substr($givenDate, 8, 2));
            $data['godzina'] = intval(substr($givenDate, 11, 2));
            $data['minuty'] = intval(substr($givenDate, 14, 2));
            $remindDate = \Carbon\Carbon::createFromDate($data['rok'], $data['miesiac'], $data['dzien']);
            $remindDate->hour = $data['godzina'];
            $remindDate->minute = $data['minuty'];
            if(\Carbon\Carbon::now()->diffInHours($remindDate) < 1) {
                $user = User::find($reminder->id_uzytkownika);

                Mail::send('emails.reminder', array('tekst' => $reminder->tresc), function($message) use($user) {
                    $message->to('sendtest@interia.pl', $user->imie." ".$user->nazwisko)
                            ->from('vouss121@gmail.com')
                            ->subject($user->telefon);
                });
                $reminder->delete();
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
