<?php

class SubmissionsController extends \BaseController {

    public function login() {
        return View::make('users.login');
    }

    public function index() {

////        $users = new User();
////        Mail::send('emails.reminder', array('tekst' => 'sdasdasds'), function($message) use($user) {
//////                    $message->to($user->email, $user->imie." ".$user->nazwisko)->subject('Reminder!');
////            $message->to('vouss121@gmail.com', $user->imie." ".$user->nazwisko)->subject('Reminder!');
////        });
////        return View::make('pages.reminders.all')->with('reminders', Reminder::with('users')->get());
//
//        Mail::send('emails.reminder', array('tekst' => 'sdasdasds'), function($message) {
//            $message->to('vouss121@gmail.com', 'John Smith')->subject('Welcome!');
//        });
//
//        return View::make('pages.users.index')->with('users', User::with('groups')->get());
    }

    public function show() {
        return View::make('pages.submissions.submissions')->with('submissions', Submission::with('orders')->get());
    }

    public function add() {
        if(Request::isMethod('get')) {
            $orders = new Order();
            return View::make('pages.submissions.add')->with('orders', $orders->showAll(1));
        } elseif(Request::isMethod('post')) {

            $input = Input::all();
            Validator::extend('greater_than', function($attribute, $value, $parameters) {
                $other = Input::get('order');

                return ($other>0)? true : false;
            });

            $validator = Validator::make($input, array(
                'order'   => 'required|greater_than',
            ));

            if($validator->fails()) {
                return Redirect::to('submissions/add')->withErrors($validator);
            }

            $submission = New Submission;
            $submission->id_uzytkownika = Auth::user()->id_uzytkownika;
            $submission->id_uslugi = Input::get('order');
            $submission->created_at = date('Y-m-d H:i:s');
            $submission->save();

            return Redirect::to('submissions');
        }
    }
}


