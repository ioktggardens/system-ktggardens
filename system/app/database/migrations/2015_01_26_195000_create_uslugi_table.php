<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUslugiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('uslugi', function(Blueprint $table) {
            $table->increments('id_uslugi');
            $table->string('nazwa');
            $table->string('opis');
            $table->float('cena_min');
            $table->float('cena_max');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
