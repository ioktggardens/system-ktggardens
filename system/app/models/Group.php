a<?php

class Group extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'grupy';
    protected $primaryKey = 'id_grupy';

    public function users() {
        return $this->hasMany('Group', 'id_grupy', 'id_grupy');
    }
    public function showAll($list = null) {
        if($list) {
            return Group::lists('nazwa', 'id_grupy');
        }

        return Group::all();
    }
}
