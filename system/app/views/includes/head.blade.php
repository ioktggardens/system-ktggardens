<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keyword" content="">
<title>KTGGardens System</title>
<!-- Bootstrap core CSS -->
{{ HTML::style('assets/css/bootstrap.css') }}
{{ HTML::style('assets/css/bootstrap-datetimepicker.min.css') }}
<!--external css-->
{{ HTML::style('assets/font-awesome/css/font-awesome.css') }}

<!-- Custom styles for this template -->
{{ HTML::style('assets/css/style.css') }}
{{ HTML::style('assets/css/style-responsive.css') }}
{{ HTML::style('assets/css/table-responsive.css') }}

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
