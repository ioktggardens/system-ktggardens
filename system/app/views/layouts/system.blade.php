<!DOCTYPE html>
    <head>
        @include('includes.head')
    </head>
    <body>
    <section id="container" >
        @include('includes.scripts')
        @include('includes.topbar')

        <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <aside>
            <div id="sidebar"  class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu" id="nav-accordion">
                    @if (Auth::user()->id_grupy == 6)
                        @include('includes.elements.menuClient')
                    @else
                        @include('includes.elements.menuAdmin')
                    @endif
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->

        <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    @yield('content')
                </div><! --/row -->
            </section>
        </section>
        <!--main content end-->
        <!--footer start-->
        <footer class="site-footer">
            <div class="text-center">
                IO project 2014/2015 | Marcin Boś & Tomasz Aarsman
            </div>
        </footer>
        <!--footer end-->
    </section>
    </body>
</html>