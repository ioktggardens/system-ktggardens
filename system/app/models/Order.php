<?php

class Order extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'uslugi';
    protected $primaryKey = 'id_uslugi';
    public $timestamps = false;

    public function submissions() {
        return $this->hasMany('Submission', 'id_uslugi', 'id_uslugi');
    }

    public function showAll($list = null) {
        $services = Order::all();
        $serviceOptions = array();
        $serviceOptions[0] = "Nazwa usługi | Opis usługi | Zakres ceny";
        foreach($services as $service){
            $serviceOptions[$service->id_uslugi] = $service->nazwa.' | '. $service->opis . ' | ' . $service->cena_min . '-' . $service->cena_max;
        }
        if($list) {
            return $serviceOptions;
        }

        return Order::all();
    }

}
