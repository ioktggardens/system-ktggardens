@extends('layouts.auth')
@section('content')
    @if(Session::has('flash_error'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_error') }}
        </div>
    @endif
    <div id="login-page">
        <div class="container">
            {{ Form::open(array('route' => 'sessions.store', 'class' => 'form-login')) }}
            <h2 class="form-login-heading">KTGGardens SYSTEM</h2>
            <div class="login-wrap">
                {{ Form::email('email', '',  array('class' => 'form-control', 'placeholder' => 'E-mail', 'required' => 'required')) }}<br />
                {{ Form::password('password',  array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required')) }}
                <br/>
                <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                {{--<hr>--}}
                {{--<div class="registration">--}}
                    {{--Don't have an account yet?<br/>--}}
                    {{--<a href="{{URL::to('/register')}}">--}}
                        {{--Create an account--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop