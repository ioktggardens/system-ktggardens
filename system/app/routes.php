<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::bind('reminder', function($value, $route) {
    return Reminder::where('id_przypomnienia', $value)->first();
});

Route::get('/login', 'SessionsController@create');
Route::get('/', 'SessionsController@create');
Route::group(array('before' => 'auth'), function() {
    Route::get('system', 'HomeController@system');
    Route::get('logout', 'SessionsController@destroy');
    Route::get('submissions/add', 'SubmissionsController@add');
    Route::post('submissions/add', 'SubmissionsController@add');
    Route::get('submissions', array('as' => 'showUserSubmissions', 'uses' => 'SubmissionsController@show'));
});
Route::group(array('before' => 'auth|admin'), function() {
    Route::get('users', 'UsersController@index');
    Route::get('users/add', 'UsersController@add');
    Route::post('users/add', 'UsersController@add');
    Route::get('users/delete/{user}', array('as' => 'deleteUser', 'uses' => 'UsersController@remove'));
    Route::get('reminders', 'RemindersController@index');
    Route::get('reminders/add', 'RemindersController@add');
    Route::post('reminders/add', 'RemindersController@add');
    Route::get('reminders/delete/{reminder}', array('as' => 'deleteReminder', 'uses' => 'RemindersController@remove'));
});

Route::resource('sessions', 'SessionsController');
Route::resource('sessions', 'SessionsController');