<?php
class RemindersController extends \BaseController {
    static $i=0;
    /**
     * Show all reminders in system
     * GET /reminders
     * @return mixed
     */
    public function index() {

//        return View::make('pages.reminders.all')->with('reminders', Reminder::find(1)->users);
        return View::make('pages.reminders.all')->with('reminders', Reminder::with('users')->get());
    }

    public function add() {
        if(Request::isMethod('get')) {
            $users = new User();
            return View::make('pages.reminders.add')->with('users', $users->showAll(1));
        } elseif(Request::isMethod('post')) {
            $rules = array('content' => 'required|between:5,200',
                            'choosenDate' => 'required');
            $validator = Validator::make(Input::all(), $rules);

            if($validator->fails()) {
                return Redirect::to('/reminders/add')->withErrors($validator);
            }

            $reminder = New Reminder;
            $reminder->id_uzytkownika = Input::get('id_uzytkownika');
            $reminder->tresc = Input::get('content');
            $reminder->data_przypomnienia = Input::get('choosenDate');
            $reminder->data_utworzenia = date('Y-m-d H:i:s');
            $givenDate = $reminder -> data_przypomnienia;
            $reminder->save();
             return Redirect::to('/reminders');
        }

    }

    public function remove(Reminder $reminder) {
        $reminder->delete();

        return Redirect::to('reminders');
    }

}


