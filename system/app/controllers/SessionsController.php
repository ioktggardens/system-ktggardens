<?php

class SessionsController extends \BaseController {

    protected $layout = 'layouts.auth';
    /**
     * Display a listing of the resource.
     * GET /sessions
     *
     * @return Response
     */
    public function index() {

    }

    /**
     * Show the form for creating a new resource.
     * GET /sessions/create
     *
     * @return Response
     */
    public function create() {
        if (!Auth::check()) {
            return View::make('auth.login');
        } else {
            return Redirect::to('system');
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /sessions
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $attempt = Auth::attempt([
            'email' => $input['email'],
            'password' => $input['password']
        ]);

        if($attempt) {
            return Redirect::intended('/system');
        } else {
            return Redirect::back()->with('flash_error', 'Invalid data!');
        }
    }

    /**
     * Display the specified resource.
     * GET /sessions/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /sessions/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /sessions/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /sessions/{id}
     *
     *
     * @return Response
     */
    public function destroy()
    {
        Auth::logout();

        return Redirect::to('login');
    }

}