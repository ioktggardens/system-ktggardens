@extends('layouts.system')

@section('content')
    @if(!$errors->isEmpty())
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ "Please choose correct service."}}
        </div>
    @endif
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Send submission</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row mt">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="form-panel">
                    {{--<h4 class="mb"><i class="fa fa-angle-right"></i> Form</h4>--}}
                    {{ Form::open(array('url' => 'submissions/add', 'method' => 'post', 'class' => 'form-horizontal style-form')) }}

                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Services</label>
                        <div class="col-sm-5">
                            {{ Form::select('order', $orders, Input::old('orders')) }}
                        </div>
                    </div>
                    <input type="submit" value="Send" class="btn btn-primary" />
                </div>
                {{ Form::close() }}
            </div>
        </div><!-- col-lg-12-->
    </section>
@stop
