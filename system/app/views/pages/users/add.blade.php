@extends('layouts.system')

@section('content')
    @if(Session::has('flash_error'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_error') }}
        </div>
    @endif
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Add user</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row mt">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="form-panel">
                    {{--<h4 class="mb"><i class="fa fa-angle-right"></i> Form</h4>--}}
                    {{ Form::open(array('url' => 'users/add', 'method' => 'post', 'class' => 'form-horizontal style-form')) }}
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Name</label>
                        <div class="col-sm-5">
                            {{ Form::text('name', '', array('class' => 'form-control', 'placeholder' => 'Name', 'autofocus'=>'autofocus', 'required' => 'required')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Surname</label>
                        <div class="col-sm-5">
                            {{ Form::text('surname','', array('class' => 'form-control', 'placeholder' => 'Surname', 'required' => 'required')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Group</label>
                        <div class="col-sm-5">
                            {{ Form::select('grupa', $groups, Input::old('groups')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Phone</label>
                        <div class="col-sm-5">
                            {{ Form::number('phone', '',  array('class' => 'form-control', 'placeholder' => 'Phone', 'required' => 'required')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-5">
                            {{ Form::email('email', '',  array('class' => 'form-control', 'placeholder' => 'E-mail', 'required' => 'required')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Password</label>
                        <div class="col-sm-5">
                            {{ Form::password('pass1',  array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Password</label>
                        <div class="col-sm-5">
                            {{ Form::password('pass2',  array('class' => 'form-control', 'placeholder' => 'Valid Password', 'required' => 'required')) }}
                            <input type="hidden" name="remember_token" value="<?php echo csrf_token(); ?>">
                        </div>
                    </div>
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
                {{ Form::close() }}
            </div>
        </div><!-- col-lg-12-->
    </section>
@stop
