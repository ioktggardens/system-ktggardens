<?php

class Reminder extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'przypomnienia';
    protected $primaryKey = 'id_przypomnienia';
    public $timestamps = false;

    public function users() {
        return $this->belongsTo('User', 'id_uzytkownika', 'id_uzytkownika');
    }
}
