@extends('layouts.system')

@section('content')
    @if(Session::has('flash_error'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_error') }}
        </div>
    @endif
    <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Add reminder</h3>
        <!-- BASIC FORM ELELEMNTS -->
        <div class="row mt">
            <div class="col-lg-12">
                <div class="form-panel">
                    {{--<h4 class="mb"><i class="fa fa-angle-right"></i> Form</h4>--}}
                    {{ Form::open(array('url' => 'reminders/add', 'method' => 'post', 'class' => 'form-horizontal style-form')) }}
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">User</label>
                        <div class="col-sm-10">
                            {{ Form::select('id_uzytkownika', $users, Input::old('Users')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Reminder Date</label>
                        <div class="col-sm-5">
                            <div class="input-group date form_datetime" data-link-field="dtp_input1">
                                <input class="form-control" type="text" name="choosenDate" value="" required="required" readonly>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" id="dtp_input1" value="" /><br/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Reminder Text</label>
                        <div class="col-sm-10">
                            {{ Form::textarea('content',null,array(
                             'class' => 'form-control',
                             'placeholder' => 'Max 200 chars',
                             'size' => '20x5',
                             'required' => 'required')) }}
                        </div>
                    </div>
                        <input type="submit" value="Add" class="btn btn-primary" />
                </div>
                    {{ Form::close() }}
                </div>
            </div><!-- col-lg-12-->
        </div><!-- /row -->
    </section>
    <script type="text/javascript">
        $('.form_datetime').datetimepicker({
            autoclose: 1,
            todayHighlight: 1,
            useSeconds: false,
            minDate: new Date("January 12, 2015")
        });
    </script>
@stop