<!-- **********************************************************************************************************************************************************
TOP BAR CONTENT
*********************************************************************************************************************************************************** -->
<!--header start-->
<header class="header black-bg">
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars" data-placement="right"></div>
    </div>
    <!--logo start-->
    <a href="{{ URL::to('/system') }}" class="logo"><b>KTGGardens/</b><small>system</small></a>
    <!--logo end-->
    <div class="nav notify-row" id="top_menu">

    </div>
    <div class="top-menu">
        <ul class="nav pull-right top-menu">
            <li><a class="logout" href="{{ URL::to('logout') }}"><i class="fa fa-power-off"></i>
                    &nbsp; Logout</a></li>
        </ul>
    </div>
</header>
<!--header end-->