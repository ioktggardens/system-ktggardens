@extends('layouts.system')
@section('content')
    <section class="wrapper">
        {{--<h3><i class="fa fa-angle-right"></i>Users</h3>--}}
        <div class="row mt">
            <div class="col-lg-12">
                <div class="content-panel">
                    <h4><i class="fa fa-angle-right"></i> My submissions</h4>
                    <section id="unseen">
                        <table class="table table-bordered table-striped table-condensed text-center">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Created at</th>
                                <th>Name</th>
                                <th>About</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($submissions as $submission)
                                <tr>
                                    <td>{{ $submission->id_zgloszenia }}</td>
                                    <td>{{ $submission->created_at }}</td>
                                    <td>{{ $submission->orders->nazwa }}</td>
                                    <td>{{ $submission->orders->opis }}</td>
                                    <td>{{ $submission->orders->cena_min }} - {{ $submission->orders->cena_max }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </section>
                </div><!-- /content-panel -->
            </div><!-- /col-lg-4 -->
        </div><!-- /row -->
    </section><! --/wrapper -->
@stop