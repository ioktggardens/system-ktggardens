<?php

class Submission extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'zgloszenia';
    protected $primaryKey = 'id_przypomnienia';
    public $timestamps = false;

    public function users() {
        return $this->belongsTo('User', 'id_uzytkownika', 'id_uzytkownika');
    }

    public function orders() {
        return $this->belongsTo('Order', 'id_uslugi', 'id_uslugi');
    }


}
