@extends('layouts.system')
@section('content')
    <section class="wrapper">
            {{--<h3><i class="fa fa-angle-right"></i> Users reminders</h3>--}}
            <div class="row mt">
                <div class="col-lg-12">
                    <div class="content-panel">
                        <h4><i class="fa fa-angle-right"></i> All reminders in system</h4>
                        <section id="unseen">
                            <table class="table table-bordered table-striped table-condensed text-center">
                                <thead>
                                <tr>
                                    <th>Reminder ID</th>
                                    <th>User</th>
                                    <th>Content</th>
                                    <th>Reminder Date</th>
                                    <th>Created at</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--{{ var_dump($reminders) }}--}}

                                @foreach($reminders as $reminder)
                                    <tr>
                                        <td>{{ $reminder->id_przypomnienia }}</td>
                                        <td>{{ $reminder->users->imie }} {{ $reminder->users->nazwisko }}</td>
                                        <td>{{ $reminder->tresc }}</td>
                                        <td>{{ $reminder->data_przypomnienia }}</td>
                                        <td>{{ $reminder->data_utworzenia }}</td>
                                        <td><a onclick="(confirm('Are you sure?'))? window.location='{{ URL::Route('deleteReminder',$reminder->id_przypomnienia)}}' : false "><i class="fa fa-times tooltips" data-original-title="Remove"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </section>
                    </div><!-- /content-panel -->
                </div><!-- /col-lg-4 -->
            </div><!-- /row -->
        </section><! --/wrapper -->
@stop