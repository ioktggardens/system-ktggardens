@extends('layouts.auth.default')

@section('content')
    <div id="register-page">
        <div class="container">
            {{ Form::open(array('url' => 'login', 'method' => 'post', 'class' => 'form-register col-md-8 col-md-offset-2')) }}
            <h2 class="form-register-heading">Register</h2>
            <div class="register-wrap">
                <div class="col-md-6" style="border-right: 1px solid green">
                    {{ Form::text('name', '',  array('class' => 'form-control', 'placeholder' => 'Name', 'autofocus'=>'autofocus', 'required' => 'required')) }}<br />
                    {{ Form::text('surname', '',  array('class' => 'form-control', 'placeholder' => 'Surname', 'required' => 'required')) }}<br />
                    {{ Form::number('phone', '',  array('class' => 'form-control', 'placeholder' => 'Phone', 'required' => 'required')) }}<br />
                    {{ Form::email('email', '',  array('class' => 'form-control', 'placeholder' => 'E-mail', 'required' => 'required')) }}<br />
                    {{ Form::password('pass1',  array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required')) }}<br />
                    {{ Form::password('pass2',  array('class' => 'form-control', 'placeholder' => 'Valid Password', 'required' => 'required')) }}

                </div>
                <div class="col-md-6">
                    {{ Form::text('street', '',  array('class' => 'form-control', 'placeholder' => 'Street', 'required' => 'required')) }}<br />
                    {{ Form::number('number', '',  array('class' => 'form-control', 'placeholder' => 'Home number', 'required' => 'required')) }}<br />
                    {{ Form::text('postal', '',  array('class' => 'form-control', 'placeholder' => 'Postal code', 'required' => 'required')) }}<br />
                    {{ Form::text('city', '',  array('class' => 'form-control', 'placeholder' => 'City', 'required' => 'required')) }}<br />
                    {{ Form::text('region', '',  array('class' => 'form-control', 'placeholder' => 'Region', 'required' => 'required')) }}<br />
                    {{ Form::text('country', '',  array('class' => 'form-control', 'placeholder' => 'Country', 'required' => 'required')) }}<br />
                </div>

                <label class="checkbox"></label>
                <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-plus-square"></i> REGISTER</button>
                <hr>

                <div class="registration">
                    <a href="{{URL::to('/login')}}">
                        <i class="fa fa-location-arrow"></i> KTGsystem Login
                    </a>
                </div>

            </div>
            </form>
        </div>
    </div>
@stop